import react from "react";

const Player = [
  {
    Header: "ID",
    accessor: "id_number",
  },
  {
    Header: "Email",
    accessor: "emailinput",
  },
  {
    Header: "Username",
    accessor: "usernameinput",
  },
  {
    Header: "Experience",
    accessor: "expinput",
  },
  {
    Header: "Level",
    accessor: "levelinput",
  },
];

export default Player;

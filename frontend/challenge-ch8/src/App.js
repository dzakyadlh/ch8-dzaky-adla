import logo from "./logo.svg";
import "./App.css";
import { useState, useMemo } from "react";
import { useTable, useGlobalFilter } from "react-table";
import Player from "./components/player";

function App() {
  const [data, setData] = useState([]);
  const [id, setId] = useState("");
  const [usernameinput, setUsernameInput] = useState("");
  const [emailinput, setEmailInput] = useState("");
  const [expinput, setExpInput] = useState("");
  const [levelinput, setLevelInput] = useState("");
  const [error, setError] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);

  const columns = useMemo(() => Player, []);
  const tableHooks = (hooks) => {
    hooks.visibleColumns.push((columns) => [
      ...columns,
      {
        Cell: ({ row }) => (
          <button
            className="updateButton"
            onClick={() =>
              handleUpdate(
                row.values.id_number,
                row.values.emailinput,
                row.values.usernameinput,
                row.values.expinput,
                row.values.levelinput
              )
            }
          >
            Edit
          </button>
        ),
      },
    ]);
  };
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    setGlobalFilter,
    prepareRow,
  } = useTable({ columns, data }, useGlobalFilter, tableHooks);
  const { globalFilter } = state;

  const dataId = (myArr) => {
    return myArr.length + 1;
  };

  const handleSubmit = () => {
    const isFound = data.find((row) => row.usernameinput === usernameinput);

    if (!isUpdate) {
      if (!isFound) {
        let id_number = dataId(data);
        setData([
          ...data,
          { id_number, emailinput, usernameinput, expinput, levelinput },
        ]);
        setEmailInput("");
        setUsernameInput("");
        setExpInput("");
        setLevelInput("");
      } else {
        setError("Username already exists");
        setTimeout(() => {
          setError("");
        }, 1500);
      }
    } else {
      const dataIndex = data.findIndex((row) => row.id_number === parseInt(id));
      if (emailinput !== "") {
        data[dataIndex].emailinput = emailinput;
      }
      if (usernameinput !== "") {
        data[dataIndex].usernameinput = usernameinput;
      }
      if (expinput !== "") {
        data[dataIndex].expinput = expinput;
      }
      if (levelinput !== "") {
        data[dataIndex].levelinput = levelinput;
      }
      setId("");
      setData([...data]);
      setEmailInput("");
      setUsernameInput("");
      setExpInput("");
      setLevelInput("");
      setIsUpdate(false);
    }
  };

  const handleUpdate = (id, email, username, exp, level) => {
    setIsUpdate(true);
    setId(id);
    setEmailInput(email);
    setUsernameInput(username);
    setExpInput(exp);
    setLevelInput(level);
  };

  return (
    <div className="App">
      <div className="container-search">
        <input
          value={globalFilter || ""}
          onChange={(e) => setGlobalFilter(e.target.value)}
          className="searchBox"
          placeholder="search"
        ></input>
      </div>
      <div className="container-main">
        <div className="container">
          <div className="row">
            <div className="col">
              <label>Email</label>
            </div>
            <div className="col">
              <input
                value={emailinput}
                onChange={(a) => setEmailInput(a.target.value)}
                className="datainput"
              ></input>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <label>Username</label>
            </div>
            <div className="col">
              <input
                value={usernameinput}
                onChange={(b) => setUsernameInput(b.target.value)}
                className="datainput"
              ></input>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <label>Experience</label>
            </div>
            <div className="col">
              <input
                value={expinput}
                onChange={(c) => setExpInput(c.target.value)}
                className="datainput"
              ></input>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <label>Level</label>
            </div>
            <div className="col">
              <input
                value={levelinput}
                onChange={(d) => setLevelInput(d.target.value)}
                className="datainput"
              ></input>
            </div>
          </div>
          <button onClick={handleSubmit} className="submitButton">
            Submit
          </button>
          {error && <div className="errorMsg">{error}</div>}
        </div>
        <div className="container">
          <table {...getTableProps()}>
            <thead>
              {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <th {...column.getHeaderProps()}>
                      {column.render("Header")}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {rows.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;

{
  "openapi": "3.0.3",
  "info": {
    "title": "Player Database",
    "description": "This is player database API for Binar FSW Challenge CH8",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "apiteam@swagger.io"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.11"
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  },
  "servers": [
    {
      "url": "http://localhost:5000"
    }
  ],
  "tags": [
    {
      "name": "player",
      "description": "This is for player API"
    }
  ],
  "paths": {
    "/api/players": {
      "get": {
        "tags": ["player"],
        "summary": "Get all players",
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerResp200"
                }
              }
            }
          },
          "400": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerResp500"
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": ["player"],
        "summary": "Create new player",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/createPlayerBody"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/createPlayerResp201"
                }
              }
            }
          },
          "500": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/createPlayerResp500"
                }
              }
            }
          }
        }
      }
    },
    "/api/players/{conditions}": {
      "get": {
        "tags": ["player"],
        "summary": "Get all players by a condition",
        "parameters": [
          {
            "name": "conditions",
            "in": "path",
            "description": "Condition of the player",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerResp200"
                }
              }
            }
          },
          "400": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerResp500"
                }
              }
            }
          }
        }
      }
    },
    "/api/players/exp/{id}": {
      "post": {
        "tags": ["player"],
        "summary": "Add player experience by certain amount",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of player to add experience",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/addPlayerExpBody"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/addPlayerExpResp200"
                }
              }
            }
          },
          "400": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/addPlayerExpResp400"
                }
              }
            }
          },
          "500": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/addPlayerExpResp500"
                }
              }
            }
          }
        }
      }
    },
    "/api/players/{id}": {
      "get": {
        "tags": ["player"],
        "summary": "Find player by ID",
        "description": "Returns a single player",
        "operationId": "findById",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of player to return",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerByIdResp200"
                }
              }
            }
          },
          "500": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/getPlayerByIdResp500"
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": ["player"],
        "summary": "Update player by ID",
        "operationId": "update",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of the player that needs to be updated",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/updatePlayerBody"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/updatePlayerResp200"
                }
              }
            }
          },
          "400": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/updatePlayerResp400"
                }
              }
            }
          },
          "500": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/updatePlayerResp500"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": ["player"],
        "summary": "Delete player by ID",
        "operationId": "delete",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of the player that needs to be deleted",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "SUCCESS",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/deletePlayerResp200"
                }
              }
            }
          },
          "400": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/deletePlayerResp400"
                }
              }
            }
          },
          "500": {
            "description": "FAILED",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/deletePlayerResp500"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "getPlayerResp200": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string"
          },
          "data": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer",
                "format": "int64",
                "example": 1
              },
              "username": {
                "type": "string",
                "example": "admin"
              },
              "email": {
                "type": "string",
                "example": "admin"
              },
              "experience": {
                "type": "integer",
                "format": "int64",
                "example": 99999
              },
              "level": {
                "type": "integer",
                "format": "int64",
                "example": 100
              }
            }
          }
        }
      },
      "getPlayerResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Some error occured while retrieving players"
          }
        }
      },
      "createPlayerBody": {
        "type": "object",
        "properties": {
          "username": {
            "type": "string",
            "example": "admin"
          },
          "email": {
            "type": "string",
            "example": "admin"
          },
          "password": {
            "type": "string",
            "example": "admin"
          },
          "experience": {
            "type": "integer",
            "format": "int64",
            "example": 99999
          },
          "level": {
            "type": "integer",
            "format": "int64",
            "example": 100
          }
        }
      },
      "createPlayerResp201": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string"
          },
          "data": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer",
                "format": "int64",
                "example": 1
              },
              "username": {
                "type": "string",
                "example": "admin"
              },
              "email": {
                "type": "string",
                "example": "admin"
              },
              "password": {
                "type": "string",
                "example": "admin"
              },
              "experience": {
                "type": "integer",
                "format": "int64",
                "example": 99999
              },
              "level": {
                "type": "integer",
                "format": "int64",
                "example": 100
              }
            }
          }
        }
      },
      "createPlayerResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Some error occured while creating the Player"
          }
        }
      },
      "addPlayerExpBody": {
        "type": "object",
        "properties": {
          "experience": {
            "type": "integer",
            "format": "int64",
            "example": 99999
          }
        }
      },
      "addPlayerExpResp200": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Player with id = 1 has more experience."
          }
        }
      },
      "addPlayerExpResp400": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Cannot update Player with id = 1!"
          }
        }
      },
      "addPlayerExpResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Error updating Player exp with id = 1"
          }
        }
      },
      "getPlayerByIdResp200": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string"
          },
          "data": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer",
                "format": "int64",
                "example": 1
              },
              "username": {
                "type": "string",
                "example": "admin"
              },
              "email": {
                "type": "string",
                "example": "admin"
              },
              "experience": {
                "type": "integer",
                "format": "int64",
                "example": 99999
              },
              "level": {
                "type": "integer",
                "format": "int64",
                "example": 100
              }
            }
          }
        }
      },
      "getPlayerByIdResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Error retrieving Player with id = 1"
          }
        }
      },
      "updatePlayerBody": {
        "type": "object",
        "properties": {
          "username": {
            "type": "string",
            "example": "admin"
          },
          "email": {
            "type": "string",
            "example": "admin"
          },
          "password": {
            "type": "string",
            "example": "admin"
          },
          "experience": {
            "type": "integer",
            "format": "int64",
            "example": 99999
          },
          "level": {
            "type": "integer",
            "format": "int64",
            "example": 100
          }
        }
      },
      "updatePlayerResp200": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string"
          },
          "data": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer",
                "format": "int64",
                "example": 1
              },
              "username": {
                "type": "string",
                "example": "admin"
              },
              "email": {
                "type": "string",
                "example": "admin"
              },
              "password": {
                "type": "string",
                "example": "admin"
              },
              "experience": {
                "type": "integer",
                "format": "int64",
                "example": 99999
              },
              "level": {
                "type": "integer",
                "format": "int64",
                "example": 100
              }
            }
          }
        }
      },
      "updatePlayerResp400": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Cannot update Player with id = 1. Maybe Player was not found or req.body is empty!"
          }
        }
      },
      "updatePlayerResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Error updating Player with id = 1"
          }
        }
      },
      "deletePlayerResp200": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Player was deleted successfully!"
          }
        }
      },
      "deletePlayerResp400": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Cannot delete Player with id = 1. Maybe Player was not found!"
          }
        }
      },
      "deletePlayerResp500": {
        "type": "object",
        "properties": {
          "status": {
            "type": "string",
            "example": "Could not delete Player with id = 1"
          }
        }
      }
    }
  }
}
